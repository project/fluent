<?php

namespace Drupal\Tests\fluent\Kernel;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\user\Entity\User;

/**
 * Paragraph test.
 */
class ParagraphTest extends KernelBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('comment', ['comment_entity_statistics']);
    $this->installSchema('node', ['node_access']);
  }

  /**
   * Fields into paragraphs.
   */
  public function testBasicTitle() {
    $node = $this->getNode();
    $this->addParagraphHero($node);
    $this->addParagraphHero($node);

    $paragraphs = [];
    $userIds = [];
    $lastUserEmail = '';
    foreach ($node->field_p_paragraph_multi->getValue() as $i => $element) {

      $paragraph = Paragraph::load($element['target_id']);
      $this->assertSame(
        (int) $paragraph->id(),
        using($node)->nthValue('field_p_paragraph_multi.id', $i)
      );

      foreach ($paragraph->field_profile->getValue() as $j => $userElement) {
        $user = User::load($userElement['target_id']);
        $userIds[] = (int) $user->id();
        $lastUserEmail = $user->getEmail();
      }
    }

    // Check if exist the user email after getting the profiles.
    $this->assertTrue(
      using($node)
        ->values('field_p_paragraph_multi.field_profile')
        ->contains('email', '=', $lastUserEmail)
    );

    $ids = using($node)
      ->values('field_p_paragraph_multi.field_profile.uid')
      ->toArray();
    $this->assertSame($userIds, $ids);

    $paragraphCount = $node->field_p_paragraph_multi->count();
    $fluentCount = using($node)->values('field_p_paragraph_multi')->count();
    $this->assertSame(
      $paragraphCount,
      $fluentCount
    );
  }

}
