<?php

namespace Drupal\Tests\fluent\Kernel;

use Drupal\block_content\Entity\BlockContent;

/**
 * Test base block fields.
 *
 * @group fluent
 */
class BlockBaseTest extends KernelBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->enableModules(['filter']);
    $this->installEntitySchema('block_content');
  }

  /**
   * Test base block fields.
   */
  public function testBaseFields() {
    $block = BlockContent::create([
      'info' => 'block info',
      'type' => 'basic',
      'body' => [
        'summary' => $this->randomGenerator->string(20),
        'value' => $this->randomGenerator->string(20),
        'format' => 'basic_html',
      ],
    ]);
    $block->save();

    $valuesFromFluent = using($block)
      ->values([
        'body_value' => 'body.value',
        'body_summary' => 'body.summary',
        'body_format' => 'body.format',
        'langcode' => 'langcode',
        'bundle' => 'bundle',
        // phpcs:ignore
        'info',
      ]);

    $body = $block->get('body')->first()->getValue();

    $this->assertSame($body['value'], $valuesFromFluent->get('body_value'));
    $this->assertSame($body['summary'], $valuesFromFluent->get('body_summary'));

    $this->assertSame($block->get('langcode')->getValue()[0]['value'], $valuesFromFluent->get('langcode'));
    $this->assertSame($block->bundle(), $valuesFromFluent->get('bundle'));
    $this->assertSame($block->get('info')->getValue()[0]['value'], $valuesFromFluent->get('info'));

    $this->assertNull(using($block)->value('field_doesnt_exist'));
    $this->assertNull(using($block)->values('field_doesnt_exist')->get('field_doesnt_exist'));
  }

}
