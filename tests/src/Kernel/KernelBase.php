<?php

namespace Drupal\Tests\fluent\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Basic installation to run fluent test.
 */
class KernelBase extends KernelTestBase {
  use ContentGeneration;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    // Modules for core functionality.
    'system',
    'user',
    'field',
    'image',
    'media',
    'file',
    'node',
    'block',
    'block_content',
    'comment',
    'datetime',
    'paragraphs',
    'link',
    'menu_link_content',
    'menu_ui',
    'options',
    'path',
    'path_alias',
    'shortcut',
    'datetime',
    'datetime_range',
    'taxonomy',
    'text',
    'entity_reference_revisions',
    'color_field',
    'webform',

    // This custom module.
    'fluent_test',
    'fluent',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('media');
    $this->installEntitySchema('paragraph');
    $this->installConfig([
      'node',
      'block_content',
      'comment',
      'fluent_test',
    ]);
  }

}
