<?php

namespace Drupal\Tests\fluent\Kernel;

use Drupal\file\Entity\File;

/**
 * Media field test.
 *
 * @group fluent
 */
class MediaTest extends KernelBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installSchema('file', ['file_usage']);
  }

  /**
   * Test Media base fields.
   */
  public function testBaseMediaFields(): void {
    $media = $this->getMediaImage();

    $this->assertInstanceOf(File::class, using($media)->value('field_media_image'));
    $this->assertEquals('Alt image', using($media)->value('field_media_image.alt'));
    $this->assertEquals('Title image', using($media)->value('field_media_image.title'));
    $this->assertEquals(500, using($media)->value('field_media_image.width'));
    $this->assertEquals(400, using($media)->value('field_media_image.height'));
  }

}
