<?php

namespace Drupal\fluent\Exception;

/**
 * Custom exception to handle field not found.
 */
class NotFieldFoundException extends \Exception {
}
