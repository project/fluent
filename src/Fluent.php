<?php

namespace Drupal\fluent;

use Drupal\Core\Entity\EntityBase;
use Drupal\options\Plugin\Field\FieldType\ListItemBase;
use Illuminate\Support\Collection as LaravelCollection;

/**
 * Magic class to solve field accessing.
 */
class Fluent {

  /**
   * Entity.
   */
  private mixed $entity;

  /**
   * Constructor.
   */
  public function __construct(
      /**
       * Field resolver.
       */
      private readonly FieldResolver $fieldResolver,
      /**
       * Field item resolver.
       */
      private readonly FieldItemResolver $fieldItemResolver
  ) {
  }

  /**
   * Set the current entity.
   *
   * @param mixed $entity
   *   Fieldable entity.
   */
  public function using(mixed $entity): Fluent {
    $this->entity = $entity;

    return $this;
  }

  /**
   * Obtain the first value in the field.
   *
   * @param string $path
   *   Use dot notation to get the field value.
   * @param mixed $defaultValue
   *   The default value if field is NULL.
   * @param bool $fail
   *   Configure the error display.
   *
   * @return mixed
   *   The field value or default.
   */
  public function value(string $path, mixed $defaultValue = NULL, bool $fail = FALSE) {
    $resolution = $this->fieldResolution($path, $this->fieldControl(0, FALSE, $path, $fail), $defaultValue);
    return $resolution->getValue();
  }

  /**
   * Get the first value in a multi value field.
   *
   * @param string $path
   *   Use dot notation to get the field value.
   * @param mixed|null $defaultValue
   *   The default value if field is NULL.
   * @param bool $fail
   *   Configure the error display.
   *
   * @return \Drupal\fluent\Collection
   *   Collection of values.
   */
  public function firstValue(string $path, $defaultValue = NULL, bool $fail = FALSE) {
    return $this->nthValue($path, 0, $defaultValue, $fail);
  }

  /**
   * Get all values in field.
   *
   * @param array|string $path
   *   Use dot notation to get the field value or array of paths for multiples.
   * @param bool $fail
   *   Configure the error display.
   *
   * @return \Drupal\fluent\Collection
   *   Collection of values.
   */
  public function values(array|string $path, bool $fail = FALSE): Collection {
    if (is_array($path)) {
      $fields = Collection::make($path);

      return $fields->mapWithKeys(function (string $path, string $alias) use ($fail) {
        $fieldValue = $this->nthValue($path, -1, NULL, $fail);

        $value = $fieldValue instanceof Collection ? $fieldValue->all() : $fieldValue;

        $rename = is_numeric($alias) ? $path : $alias;

        return [
          $rename => $value,
        ];
      });
    }
    else {
      $values = $this->nthValue($path, -1, NULL, $fail);

      if (!$values instanceof LaravelCollection) {
        return Collection::make([
          $path => $values,
        ]);
      }

      return $values;
    }
  }

  /**
   * Return the NTH position in the field.
   *
   * @param string $path
   *   Use dot notation to get the field value.
   * @param int $position
   *   Element possition.
   * @param mixed|null $defaultValue
   *   Default value.
   * @param bool $fail
   *   Configure the error display.
   *
   * @return string|\Drupal\fluent\Collection
   *   Collection of values.
   */
  public function nthValue(string $path, int $position, $defaultValue = NULL, bool $fail = FALSE) {
    $isMultivalued = FALSE;
    if ($position === -1) {
      $isMultivalued = TRUE;
    }

    $fieldControl = $this->fieldControl($position, $isMultivalued, $path, $fail);
    $resolution = $this->fieldResolution($path, $fieldControl, $defaultValue);

    $field = $resolution->getValue();

    $collection = $field instanceof EntityBase ? [$field] : $field;

    if (is_array($collection)) {
      $fluentCollection = Collection::make($collection);

      // Flatten values if is a list.
      try {
        $class = $resolution->getFieldDefinition()->getItemDefinition()->getClass();
        $fieldClass = new \ReflectionClass($class);
        if ($fieldClass->isSubclassOf(ListItemBase::class)) {
          return $fluentCollection->mapWithKeys(fn($value) => $value);
        }
      }
      catch (\Exception) {
      }
      return $fluentCollection;
    }
    else {
      return $collection;
    }
  }

  /**
   * Resolve field value.
   */
  private function fieldResolution($path, FieldControl $fieldControl, $defaultValue): FieldResolution {
    $resolution = $this->fieldResolver->resolve($this->entity, $fieldControl);
    return $this->fieldItemResolver->resolve($resolution, $this->getPath($path), $defaultValue);
  }

  /**
   * Convert path to array.
   */
  private function getPath(string $path): array {
    $path = preg_replace('/[^a-zA-Z\d._]/', '', $path);
    return explode('.', $path);
  }

  /**
   * Object with the information to control the field accessing.
   */
  private function fieldControl(int $position, bool $isMultivalued, string $path, $fail): FieldControl {
    $fieldControl = new FieldControl();
    $fieldControl
      ->setMultivalued($isMultivalued)
      ->setPosition($position)
      ->setErrorDisplay($fail)
      ->setPath($path);

    return $fieldControl;
  }

}
