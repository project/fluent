<?php

namespace Drupal\fluent;

use Drupal\Core\Entity\ContentEntityInterface;
use Illuminate\Support\Collection as BaseCollection;

/**
 * Customize method in Laravel Collection to be compatibles with Drupal.
 */
class Collection extends BaseCollection {

  /**
   * {@inheritdoc}
   */
  protected function operatorForWhere($key, $operator = NULL, $value = NULL) {
    if ($this->useAsCallable($key)) {
      return $key;
    }

    if (func_num_args() === 1) {
      $value = TRUE;

      $operator = '=';
    }

    if (func_num_args() === 2) {
      $value = $operator;

      $operator = '=';
    }

    return function ($item) use ($key, $operator, $value) {
      if ($item instanceof ContentEntityInterface) {
        $retrieved = using($item)->value($key);
      }
      else {
        $retrieved = data_get($item, $key);
      }

      $strings = array_filter([$retrieved, $value], fn($value) => is_string($value) || (is_object($value) && method_exists($value, '__toString')));

      if (count($strings) < 2 && count(array_filter([$retrieved, $value], 'is_object')) == 1) {
        return in_array($operator, ['!=', '<>', '!==']);
      }

      switch ($operator) {
        default:
        case '=':
        case '==':
          return $retrieved == $value;
        case '!=':
        case '<>':
          return $retrieved != $value;

        case '<':
          return $retrieved < $value;

        case '>':
          return $retrieved > $value;

        case '<=':
          return $retrieved <= $value;

        case '>=':
          return $retrieved >= $value;

        case '===':
          return $retrieved === $value;

        case '!==':
          return $retrieved !== $value;
      }
    };
  }

}
