<?php

namespace Drupal\fluent;

use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * State class to save each resolution.
 */
class FieldResolution {

  /**
   * Current field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  private FieldDefinitionInterface $fieldDefinition;

  /**
   * Current field.
   *
   * @var mixed
   */
  private $field;

  /**
   * Current value.
   *
   * @var mixed
   */
  private $value;

  /**
   * Parent of current field.
   *
   * @var mixed
   */
  private $parent;

  /**
   * Control the force skip.
   *
   * @var bool
   */
  private bool $forceSkip = FALSE;

  /**
   * Current value.
   *
   * @return mixed
   *   Current value.
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Set current value.
   *
   * @param mixed $value
   *   Field value.
   */
  public function setValue(mixed $value): FieldResolution {
    $this->value = $value;
    return $this;
  }

  /**
   * Get current parent.
   *
   * @return mixed
   *   Parent.
   */
  public function getParent() {
    return $this->parent;
  }

  /**
   * Set parent.
   *
   * @param mixed $parent
   *   Parent.
   */
  public function setParent(mixed $parent): FieldResolution {
    $this->parent = $parent;
    return $this;
  }

  /**
   * Get field definition.
   *
   * @return mixed
   *   Current field definition.
   */
  public function getFieldDefinition() {
    return $this->fieldDefinition;
  }

  /**
   * Set field definition.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   Field definition.
   */
  public function setFieldDefinition(FieldDefinitionInterface $fieldDefinition): FieldResolution {
    $this->fieldDefinition = $fieldDefinition;
    return $this;
  }

  /**
   * Get field.
   *
   * @return mixed
   *   Current field.
   */
  public function getField() {
    return $this->field;
  }

  /**
   * Set field.
   *
   * @param mixed $field
   *   Current field.
   */
  public function setField(mixed $field): FieldResolution {
    $this->field = $field;
    return $this;
  }

  /**
   * Force skip if field needs to be resolved.
   *
   * @param bool $force
   *   The force.
   *
   * @return $this
   */
  public function forceSkip(bool $force): self {
    $this->forceSkip = $force;
    return $this;
  }

  /**
   * Needs To Force Skip.
   */
  public function needsToForceSkip(): bool {
    return $this->forceSkip;
  }

}
