<?php

namespace Drupal\fluent\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Fluent field resolver plugin base class.
 */
abstract class FluentFieldResolverBase extends PluginBase implements FluentFieldResolverInterface {

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityType;

  /**
   * {@inheritdoc}
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $em) {
    $this->entityType = $em;
  }

}
