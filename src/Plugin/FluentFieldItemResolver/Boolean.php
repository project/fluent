<?php

namespace Drupal\fluent\Plugin\FluentFieldItemResolver;

use Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem;
use Drupal\fluent\Plugin\FluentFieldItemResolverPluginBase;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "boolean",
 *   label = @Translation("Boolean field"),
 * )
 */
class Boolean extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof BooleanItem;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field): bool {
    return $field->getString() == 1;
  }

}
