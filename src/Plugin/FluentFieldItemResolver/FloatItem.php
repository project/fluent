<?php

namespace Drupal\fluent\Plugin\FluentFieldItemResolver;

use Drupal\Core\Field\Plugin\Field\FieldType\DecimalItem;
use Drupal\Core\Field\Plugin\Field\FieldType\FloatItem as FloatItemField;
use Drupal\Core\TypedData\Plugin\DataType\FloatData;
use Drupal\fluent\Plugin\FluentFieldItemResolverPluginBase;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "float",
 *   label = @Translation("Float field"),
 * )
 */
class FloatItem extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof FloatItemField
      || $field instanceof FloatData
      || $field instanceof DecimalItem;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field): float {
    return (float) $field->getString();
  }

}
