<?php

namespace Drupal\fluent\Plugin\FluentFieldItemResolver;

use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem as IntegerItemField;
use Drupal\fluent\Plugin\FluentFieldItemResolverPluginBase;

/**
 * Plugin implementation of the fluent_field_item_resolver.
 *
 * @FluentFieldItemResolver(
 *   id = "integer",
 *   label = @Translation("Integer field"),
 * )
 */
class Integer extends FluentFieldItemResolverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function can($field): bool {
    return $field instanceof IntegerItemField;
  }

  /**
   * {@inheritdoc}
   */
  public function handler($field): int {
    return (int) $field->getString();
  }

}
