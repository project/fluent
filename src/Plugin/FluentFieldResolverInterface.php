<?php

namespace Drupal\fluent\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\fluent\FieldControl;

/**
 * Defines an interface for Fluent field resolver plugins.
 */
interface FluentFieldResolverInterface extends PluginInspectionInterface {

  /**
   * Determinate if can handle the field type.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   Current field definition.
   *
   * @return bool
   *   True if this plugin can handle the current field.
   */
  public function can(FieldDefinitionInterface $fieldDefinition): bool;

  /**
   * Method to handle field.
   *
   * @param mixed $object
   *   Current field.
   * @param string $fieldName
   *   Current field name.
   * @param \Drupal\fluent\FieldControl $fieldControl
   *   Field control.
   *
   * @return mixed
   *   Field item object.
   */
  public function handler($object, $fieldName, FieldControl $fieldControl);

  /**
   * Set the entity type manager in the plugin instance creation.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $em
   *   Drupal entity type manager.
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $em);

}
