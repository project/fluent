<?php

namespace Drupal\fluent\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\fluent\Annotation\FluentFieldResolver;

/**
 * Provides the Fluent field resolver plugin manager.
 */
class FluentFieldResolverManager extends DefaultPluginManager {

  /**
   * Constructs a new FluentFieldResolverManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $etm
   *   Entity type manager to inject on each plugin instance.
   */
  public function __construct(\Traversable $namespaces,
  CacheBackendInterface $cache_backend,
  ModuleHandlerInterface $module_handler, /**
                                           * Entity type manager.
                                           */
  private readonly EntityTypeManagerInterface $etm) {
    parent::__construct(
          'Plugin/FluentFieldResolver',
          $namespaces,
          $module_handler,
          FluentFieldResolverInterface::class,
          FluentFieldResolver::class
      );
    $this->alterInfo('fluent_fluent_field_resolver_info');
    $this->setCacheBackend($cache_backend, 'fluent_fluent_field_resolver_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin = parent::createInstance($plugin_id, $configuration);

    if ($plugin instanceof FluentFieldResolverInterface) {
      $plugin->setEntityTypeManager($this->etm);
    }

    return $plugin;
  }

}
