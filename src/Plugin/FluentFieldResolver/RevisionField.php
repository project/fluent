<?php

namespace Drupal\fluent\Plugin\FluentFieldResolver;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\fluent\FieldControl;
use Drupal\fluent\Plugin\FluentFieldResolverBase;

/**
 * Load revision fields.
 *
 * @FluentFieldResolver(
 *   id = "revision_field",
 *   label = "Entity Revision Field",
 *   weight = 100
 * )
 */
class RevisionField extends FluentFieldResolverBase {

  /**
   * {@inheritdoc}
   */
  public function handler($object, $fieldName, FieldControl $fieldControl) {
    /** @var \Drupal\Core\Entity\RevisionableStorageInterface $storage */
    $storage = $this->entityType
      ->getStorage($object->getFieldDefinition()->getTargetEntityTypeId());

    if ($fieldControl->isMultivalued()) {
      // @todo support multiple load revisions.
      // see NodeStorage:12
      if (!method_exists($storage, 'revisionIds')) {
        throw new \Exception('Don\'t support multiple revisions');
      }

      $revision = $storage->loadMultipleRevisions(
            array_reverse($storage->revisionIds($object->getParent()->getEntity()))
        );
    }
    else {
      $revision = $storage->loadRevision($object->value);
    }

    return $revision;
  }

  /**
   * {@inheritdoc}
   */
  public function can(FieldDefinitionInterface $fieldDefinition): bool {
    $revisionTargets = [
      'taxonomy_term',
      'block_content',
    ];

    return (
      ($fieldDefinition->getName() === 'vid' && $fieldDefinition->getTargetEntityTypeId() === 'node')
      ||
      ($fieldDefinition->getName() === 'revision_id' && in_array($fieldDefinition->getTargetEntityTypeId(), $revisionTargets))
    );
  }

}
