<?php

namespace Drupal\fluent\Plugin\FluentFieldResolver;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\Core\TypedData\PrimitiveBase;
use Drupal\fluent\Exception\NotFieldFoundException;
use Drupal\fluent\FieldControl;
use Drupal\fluent\Plugin\FluentFieldResolverBase;

/**
 * Common field resolver.
 *
 * @FluentFieldResolver(
 *   id = "common_field",
 *   label = "Commond Field Resolver",
 *   weight = 100
 * )
 */
class CommonField extends FluentFieldResolverBase {

  /**
   * {@inheritdoc}
   */
  public function handler($object, $fieldName, FieldControl $fieldControl) {
    $field = NULL;

    if ($object instanceof ContentEntityInterface) {
      $field = $this->resolveFieldItem($object, $fieldName, $fieldControl);
    }

    if ($object instanceof FieldItemListInterface) {
      $cardinality = $object->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();

      $items = [];
      foreach ($object as $item) {
        $items[] = $item;
      }

      if ($fieldControl->isMultivalued() && ($cardinality == -1 | $cardinality > 1)) {
        return $items;
      }

      return $object->first();
    }

    if ($object instanceof FieldItemInterface) {
      try {
        $field = $object->get($fieldName);
      }
      catch (MissingDataException) {
        // @todo Handle exception.
      }
    }

    if ($object instanceof PrimitiveBase) {
      return $object;
    }

    return $field;
  }

  /**
   * Resolve Field Item.
   */
  private function resolveFieldItem(ContentEntityInterface $object, string $fieldName, FieldControl $fieldControl) {
    // @todo refactor this code.
    // First try to access as a field
    // if not exist try to access as a method
    $field = NULL;
    try {
      $field = $object->get($fieldName);
    }
    catch (\InvalidArgumentException) {
      if ($method = $this->getMethod($object, $fieldName)) {
        $field = $object->{$method}();
      }

      if ($field === NULL && $fieldControl->showErrors()) {
        // @todo Error handling.
        $fields = array_keys($object->getFields());

        // Remove __ methods.
        $methods = get_class_methods($object);
        $methods = preg_replace('/__./', '', $methods);

        $message = sprintf(
          'Field %s not found, available fields: %s

      Available methods: %s',
          $fieldName,
          implode(', ', $fields),
          implode(', ', $methods)
        );

        throw new NotFieldFoundException($message);
      }
    }

    return $field;
  }

  /**
   * {@inheritdoc}
   */
  public function can(FieldDefinitionInterface $fieldDefinition): bool {
    return TRUE;
  }

  /**
   * Check if field exist.
   *
   * @param mixed $object
   *   Current object.
   * @param mixed $fieldName
   *   Current field name.
   *
   * @return bool
   *   True if exist.
   */
  public function hasField(mixed $object, mixed $fieldName): bool {
    return method_exists($object, 'hasField') && $object->hasField($fieldName);
  }

  /**
   * Check if method exist.
   *
   * @param mixed $object
   *   Current object.
   * @param mixed $possibleName
   *   Current field name.
   *
   * @return string|null
   *   Name or null.
   */
  public function getMethod(mixed $object, mixed $possibleName) {
    if ($this->isMethod($object, $possibleName)) {
      return $possibleName;
    }

    $prefixes = [
      'get',
      'is',
      'to',
    ];

    foreach ($prefixes as $prefix) {
      $name = $prefix . ucfirst((string) $possibleName);
      if ($this->isMethod($object, $name)) {
        return $name;
      }
    }

    return NULL;
  }

  /**
   * Check if is method.
   *
   * @param mixed $entity
   *   Current entity.
   * @param mixed $name
   *   Current field name.
   *
   * @return bool
   *   True if method exist.
   */
  public function isMethod(mixed $entity, mixed $name): bool {
    return method_exists($entity, $name);
  }

}
