<?php

namespace Drupal\fluent\Plugin\FluentFieldResolver;

use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\fluent\FieldControl;
use Drupal\fluent\Plugin\FluentFieldResolverBase;

/**
 * Entity reference resolver.
 *
 * @FluentFieldResolver(
 *   id = "entity_reference",
 *   label = "Entity Reference Resolver",
 *   weight = 90
 * )
 */
class EntityReferenceField extends FluentFieldResolverBase {

  /**
   * {@inheritdoc}
   */
  public function handler($object, $fieldName, FieldControl $fieldControl) {
    if ($object instanceof EntityReferenceFieldItemListInterface) {

      $cardinality = $object->getFieldDefinition()->getFieldStorageDefinition()->getCardinality();
      $entities = $object->referencedEntities();

      if ($fieldControl->isMultivalued() && ($cardinality == -1 | $cardinality > 1)) {
        return $entities;
      }

      $position = 0;
      if ($fieldControl->getPosition() != -1) {
        $position = $fieldControl->getPosition();
      }

      return $object->get($position);
    }

    if ($object instanceof EntityReferenceItem) {
      return $this->loadEntityUsingFieldDefinition($object, $fieldControl);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function can(FieldDefinitionInterface $fieldDefinition): bool {
    $types = [
      'entity_reference',
      'entity_reference_revisions',
      'image',
      'file',
      'webform',
    ];

    return in_array($fieldDefinition->getType(), $types);
  }

  /**
   * Resolve entities loading automatically.
   */
  public function loadEntityUsingFieldDefinition(EntityReferenceItem $object, FieldControl $fieldControl) {
    $entityRefProperties = $object->getProperties();

    if (array_key_exists($fieldControl->nextPathName(), $entityRefProperties)) {
      return $object->get($fieldControl->nextPathName());
    }

    $entityType = $object->getFieldDefinition()->getSetting('target_type');
    return $this->entityType->getStorage($entityType)
      ->load($object->target_id);
  }

}
