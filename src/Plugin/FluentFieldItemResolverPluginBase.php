<?php

namespace Drupal\fluent\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for fluent_field_item_resolver plugins.
 */
abstract class FluentFieldItemResolverPluginBase extends PluginBase implements FluentFieldItemResolverInterface {

}
