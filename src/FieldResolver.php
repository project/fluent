<?php

namespace Drupal\fluent;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\TypedData\PrimitiveBase;
use Drupal\fluent\Plugin\FluentFieldResolverManager;

/**
 * Allow to solve the field path to get the field item.
 */
class FieldResolver {

  /**
   * List of plugins.
   *
   * @var array
   */
  private array $fieldResolvers = [];

  /**
   * Current entity.
   *
   * @var mixed
   */
  private $entity;

  /**
   * Field resolution save the current state of the path.
   *
   * @var \Drupal\fluent\FieldResolution
   */
  private FieldResolution $resolution;

  /**
   * Common field resolver.
   *
   * @var \Drupal\fluent\Plugin\FluentFieldResolver\CommonField
   */
  private $commonResolver;

  /**
   * Constructor.
   */
  public function __construct(/**
                               * Plugin field resolver.
                               */
  private readonly FluentFieldResolverManager $pluginManager) {
    foreach (array_keys($this->pluginManager->getDefinitions()) as $id) {
      $this->fieldResolvers[$id] = $this->pluginManager->createInstance($id);
    }

    $this->commonResolver = $this->fieldResolvers['common_field'];
    unset($this->fieldResolvers['common_field']);
  }

  /**
   * Main method to resolve field.
   */
  public function resolve($entity, FieldControl $fieldControl): FieldResolution {
    $this->entity = $entity;
    $this->resolution = new FieldResolution();

    // Save the current entity.
    $tmpEntity = $this->entity;

    do {
      $possibleField = $fieldControl->currentPath();

      $solvedField = $this->getResolver($tmpEntity, $possibleField, $fieldControl);

      if ($solvedField === NULL) {
        break;
      }

      $tmpEntity = $solvedField;

      if ($this->resolution->needsToForceSkip()) {
        $this->resolution->forceSkip(FALSE);
        continue;
      }

      $fieldControl->moveToNextPathName();
    } while ($fieldControl->currentPath() !== NULL);

    return $this->resolution;
  }

  /**
   * If is one instance of those objects skip the count and resolve over there.
   */
  private function skipCount($solvedField): bool {
    return (
      $solvedField instanceof FieldItemListInterface
      ||
      $solvedField instanceof EntityReferenceItem
    );
  }

  /**
   * Resolve field when have multiple elements.
   */
  private function fieldIsArray(array $entity, string $fieldName, FieldControl $fieldControl) {
    $field = [];

    foreach ($entity as $item) {
      $resolvedField = $this->getResolver($item, $fieldName, $fieldControl);

      if (is_array($resolvedField) && count($resolvedField) === 1) {
        $resolvedField = reset($resolvedField);
      }

      $field[] = $resolvedField;
    }

    if (count($field) === 1 && is_array($field[0])) {
      $field = reset($field);
    }

    $this->resolution->setField($field);
    return $field;
  }

  /**
   * Obtain the field resolved by the PluginFieldResolver.
   */
  private function getResolver($entity, $fieldName, FieldControl $fieldControl) {
    if (is_array($entity)) {
      return $this->fieldIsArray($entity, $fieldName, $fieldControl);
    }

    // Is configuration.
    if ($entity instanceof ConfigEntityInterface) {
      $this->resolution->setField($entity->get($fieldName));
      return $entity->get($fieldName);
    }

    // Primitive object will resolve in the phase.
    if ($entity instanceof PrimitiveBase) {
      $this->resolution->setField($entity);
      return $entity;
    }

    $fieldDefinition = $entity->getFieldDefinition($fieldName);

    if ($fieldDefinition) {
      $this->resolution->setFieldDefinition($fieldDefinition);

      foreach ($this->fieldResolvers as $resolver) {
        if ($resolver->can($fieldDefinition)) {

          $field = $entity instanceof FieldableEntityInterface ? $entity->get($fieldName) : $entity;

          $field = $resolver->handler($field, $fieldName, $fieldControl);
          $this->resolution->setField($field);

          if ($this->skipCount($field)) {
            $this->resolution->forceSkip(TRUE);
          }

          return $field;
        }
      }
    }

    $field = $this->commonResolver->handler($entity, $fieldName, $fieldControl);
    $this->resolution->setField($field);

    if ($this->skipCount($field)) {
      $this->resolution->forceSkip(TRUE);
    }

    return $field;
  }

}
