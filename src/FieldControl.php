<?php

namespace Drupal\fluent;

/**
 * State class to save option in order to resolve the field.
 */
class FieldControl {

  /**
   * Is multivalue.
   *
   * @var bool
   */
  private bool $multivalued;

  /**
   * Item position.
   *
   * @var int
   */
  private int $position;

  /**
   * Original path request.
   *
   * @var array
   */
  private array $path;

  /**
   * Control the path position.
   *
   * @var int
   */
  private int $pathPosition = 0;

  /**
   * True to display errors, use for debug.
   *
   * @var bool
   */
  private bool $showErrors;

  /**
   * Next path name if existed.
   */
  private ?string $nextPathName = NULL;

  /**
   * Is multivalue.
   *
   * @return bool
   *   Multi value.
   */
  public function isMultivalued(): bool {
    return $this->multivalued;
  }

  /**
   * Set if user is requesting multi value field.
   *
   * @param bool $multivalued
   *   Is multi value.
   */
  public function setMultivalued(bool $multivalued): FieldControl {
    $this->multivalued = $multivalued;
    return $this;
  }

  /**
   * Get position number.
   *
   * @return int
   *   Position number.
   */
  public function getPosition(): int {
    return $this->position;
  }

  /**
   * Set position.
   *
   * @param int $position
   *   Position to obtain.
   */
  public function setPosition(int $position): FieldControl {
    $this->position = $position;
    return $this;
  }

  /**
   * Save path to resolve field.
   *
   * @param string $path
   *   Full path.
   *
   * @return $this
   */
  public function setPath(string $path): FieldControl {
    $path = preg_replace('/[^a-zA-Z\d._]/', '', $path);
    $this->path = explode('.', $path);
    return $this;
  }

  /**
   * Calculate the current field path name.
   *
   * @return string|null
   *   Current field path name.
   */
  public function currentPath(): ?string {
    if ($this->pathPosition >= count($this->path)) {
      return NULL;
    }

    $currentPathName = $this->path[$this->pathPosition];

    $this->nextPathName = $this->hasNextPathName() ? $this->path[$this->pathPosition + 1] : NULL;

    return $currentPathName;
  }

  /**
   * Return the next path name if exist.
   *
   * @return string|null
   *   Next field path name if exists.
   */
  public function nextPathName(): ?string {
    return $this->nextPathName;
  }

  /**
   * Move to the next position in the path array.
   */
  public function moveToNextPathName(): FieldControl {
    $this->pathPosition++;
    return $this;
  }

  /**
   * Check if exist a next field path name.
   */
  public function hasNextPathName(): bool {
    return ($this->pathPosition + 1) < count($this->path);
  }

  /**
   * Configure the error display.
   *
   * @param bool $fail
   *   Configure to show errors.
   */
  public function setErrorDisplay(bool $fail): FieldControl {
    $this->showErrors = $fail;
    return $this;
  }

  /**
   * Indicate if it needs to display errors.
   */
  public function showErrors(): bool {
    return $this->showErrors;
  }

}
