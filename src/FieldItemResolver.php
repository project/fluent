<?php

namespace Drupal\fluent;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\PrimitiveInterface;
use Drupal\fluent\Plugin\FluentFieldItemResolverPluginManager;

/**
 * Once get the field item resolve the value inside.
 */
class FieldItemResolver {

  /**
   * List of plugins.
   */
  private array $fieldResolvers = [];

  /**
   * Constructor.
   */
  public function __construct(
    private readonly FluentFieldItemResolverPluginManager $pluginManager
  ) {
    foreach (array_keys($this->pluginManager->getDefinitions()) as $id) {
      $this->fieldResolvers[$id] = $this->pluginManager->createInstance($id);
    }
  }

  /**
   * Main method to resolve field item.
   */
  public function resolve(FieldResolution $resolution, array $path, $defaultValue = NULL): FieldResolution {
    $field = $resolution->getField();
    $value = $this->fieldValueResolution($field, $path, $defaultValue);
    $resolution->setValue($value);

    return $resolution;
  }

  /**
   * Resolves the value of a field based on its structure.
   *
   * This method inspects the nature of the provided $field.
   * If $field is an array, it recursively resolves the values
   * for each item within the array. If the item is an array itself,
   * the method calls itself, otherwise, it fetches the value using the
   * `getValueField` method.
   *
   * @param mixed $field
   *   The field whose value needs to be resolved. Can be a
   *   scalar value or an array.
   * @param string $path
   *   The path used to fetch the value of the field.
   * @param mixed $default
   *   The default value to return if the field's value cannot be resolved.
   *
   * @return mixed
   *   Returns the resolved value. If theoriginal field is an array,
   *   returns an array of resolved values.
   */
  private function fieldValueResolution(mixed $field, $path, mixed $default): mixed {
    if (is_array($field)) {
      $value = [];
      foreach ($field as $key => $item) {
        if (is_array($item)) {
          $value[] = $this->fieldValueResolution($item, $path, $default);
        }
        else {
          $value[] = $this->getValueField($item, $path, $default);
        }
      }

      return $value;
    }

    return $this->getValueField($field, $path, $default);
  }

  /**
   * Resolve the value within field item.
   */
  private function getValueField($field, $path, $defaultValue) {
    if ($field instanceof FieldItemListInterface && $field->isEmpty()) {
      $default = $field->getFieldDefinition()->getDefaultValueLiteral();
      $end = end($path);

      if (!empty($default) && array_key_exists($end, $default[0])) {
        return $default[0][$end];
      }

      return $defaultValue;
    }

    return $this->resolveFieldValue($field, $defaultValue);
  }

  /**
   * Determinate what plugin resolve the field item.
   */
  private function resolveFieldValue($field, $defaultValue) {
    foreach ($this->fieldResolvers as $resolver) {
      if ($resolver->can($field)) {
        return $resolver->handler($field);
      }
    }

    if ($field instanceof PrimitiveInterface || $field instanceof ComplexDataInterface) {
      if ($field->getValue() === NULL || $field->getValue() === '') {
        $value = $defaultValue;
      }
      elseif ($field instanceof ComplexDataInterface) {
        $value = $field->toArray();
      }
      else {
        $value = $field->getString();
      }

      // If value is the unique key in the resolved value we extract and return
      // as unique value, if not like complex fields we return all values.
      if (is_array($value) && count($value) === 1 && array_key_exists('value', $value)) {
        $value = $value['value'];
      }

      return $value ?? $defaultValue;
    }

    return $field ?? $defaultValue;
  }

}
